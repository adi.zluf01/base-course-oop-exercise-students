package Missions;

import AerialVehicles.AerialVehicle;
import Entities.Coordinates;

public abstract class Mission{
    private Coordinates destination;
    private String pilotName;
    private AerialVehicle plane;

    public Mission(Coordinates destination, String pilotName,AerialVehicle plane){
        this.destination=destination;
        this.pilotName=pilotName;
        this.plane=plane;
    }

    public void setDestination(Coordinates destination) {
        this.destination = destination;
    }

    public void setPlane(AerialVehicle plane) {
        this.plane = plane;
    }

    public void setPilotName(String pilotName) {
        this.pilotName = pilotName;
    }

    public void begin(){
        System.out.println("Beginning Mission!");
        this.plane.flyTo(this.destination);
    }
    public void cancel() {
        System.out.println("Abort Mission!");
        plane.land(plane.getHomeBase());
    }

    public void finish(){
        System.out.println(executeMission());
        plane.land(plane.getHomeBase());
        System.out.println("Finish Mission!");
    }

    public Coordinates getDestination() {
        return destination;
    }

    public String getPilotName() {
        return pilotName;
    }

    public AerialVehicle getPlane() {
        return plane;
    }

    protected abstract String executeMission();

}
