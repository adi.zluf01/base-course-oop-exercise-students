package Missions;

import AerialVehicles.AerialVehicle;
import Entities.Coordinates;

public class AttackMission extends Mission {
    private String target;

    public AttackMission(Coordinates destination, String pilotName, AerialVehicle plane, String target) {
        super(destination, pilotName, plane);
        this.target = target;
    }

    @Override
    public String executeMission() {
        return this.getPilotName() +": "+ this.getPlane().getClass().getSimpleName() + " Attacking suspect " + this.target + " with: " + this.getPlane().getAbility();
    }
}
