package Missions;

import AerialVehicles.AerialVehicle;
import Entities.Coordinates;

public class BdaMission extends Mission {

    private String objective;

    public BdaMission(Coordinates destination, String pilotName, AerialVehicle plane, String objective) {
        super(destination, pilotName, plane);
        this.objective = objective;

    }

    @Override
    protected String executeMission() {
        return this.getPilotName() + ": " + this.getPlane().getClass().getSimpleName() + " taking pictures of suspect " + this.objective + " with: " + this.getPlane().getAbility();
    }
}
