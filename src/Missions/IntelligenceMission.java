package Missions;

import AerialVehicles.AerialVehicle;
import Entities.Coordinates;

public class IntelligenceMission extends Mission {
    private String region;

    public IntelligenceMission(Coordinates destination, String pilotName, AerialVehicle plane, String region) {
        super(destination, pilotName, plane);
        this.region=region;
    }

    @Override
    protected String executeMission() {
        return this.getPilotName() + ": " + this.getPlane().getClass().getSimpleName() + " Collecting Data in " + this.region +" "+ this.getPlane().getAbility();
    }
}
