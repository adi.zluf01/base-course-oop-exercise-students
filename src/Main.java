import AerialVehicles.unmannedVehicle.Eitan;
import AerialVehicles.fighterJets.F15;
import AerialVehicles.fighterJets.F16;
import Entities.*;
import Missions.AerialVehicleNotCompatibleException;
import Missions.AttackMission;
import Missions.BdaMission;
import Missions.IntelligenceMission;

public class Main {

    public static void main(String[] args) throws AerialVehicleNotCompatibleException {
	// Implement scenarios from readme here

        System.out.println("Inelligence mission:");
        Coordinates inelligenceCD= new Coordinates(33.2033427805222,44.5176910795946);
        Coordinates homeBaseE= new Coordinates(31.827604665263365,34.81739714569337);
        Eitan  inelligenceE=new Eitan(100,homeBaseE, Status.READYFORFLIGHT, 4, MissileType.PYTHON, SensorType.INFRARED);
        IntelligenceMission i=new IntelligenceMission(inelligenceCD, "Dror zalicha", inelligenceE , "Iraq");
        i.begin();
        inelligenceE.hoverOverLocation(inelligenceCD);
        i.finish();

        System.out.println();
        System.out.println("Attack mission:");
        F15 attackPlane =new F15(50,homeBaseE,Status.READYFORFLIGHT,250,MissileType.SPICE250, SensorType.INFRARED);
        AttackMission attack =new AttackMission(inelligenceCD, "Ze'ev Raz", attackPlane, "Tuwaitha Nuclear Research Center");
        attack.begin();
        attack.finish();

        System.out.println();
        System.out.println("Bda mission:");
        F16 BdaPlane =new F16(50,homeBaseE,Status.READYFORFLIGHT,250,MissileType.SPICE250, CameraType.THERMAL);
        BdaMission bda =new BdaMission(inelligenceCD, "Ilan Ramon", BdaPlane, "Tuwaitha Nuclear Research Center");
        bda.begin();
        bda.finish();

        System.out.println("All missions are done successfully");

    }
}
