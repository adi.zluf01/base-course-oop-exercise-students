package Entities;
//camera type
public enum CameraType {
    REGULAR,
    THERMAL,
    NIGHTVISION
}
