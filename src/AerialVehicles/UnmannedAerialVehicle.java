package AerialVehicles;

import Entities.*;

public interface UnmannedAerialVehicle{
    void hoverOverLocation(Coordinates destination);
}
