package AerialVehicles.unmannedVehicle;


import AerialVehicles.UnmannedAerialVehicle;
import AerialVehicles.planeTypes.AttackPlane;
import AerialVehicles.planeTypes.IntelligencePlane;
import AerialVehicles.planeTypes.VerificationAttacksPlane;
import Entities.*;

public class Shoval extends AttackPlane implements UnmannedAerialVehicle {
    private IntelligencePlane intelligencePlane;
    private VerificationAttacksPlane verificationAttacksPlane;

    public Shoval(int flightHour, Coordinates homeBase, Status status, int missilesAmount, MissileType missileType, SensorType sensor, CameraType camera) {
        super(flightHour, homeBase, status, Family.HARON, missilesAmount, missileType);
        this.intelligencePlane = new IntelligencePlane(flightHour, homeBase, status, Family.HARON, sensor);
        this.verificationAttacksPlane=new VerificationAttacksPlane(flightHour,homeBase,status, Family.HARON,camera);
    }

    @Override
    public void hoverOverLocation(Coordinates destination) {
        this.setFlightStatus(Status.INTHEAIR);
        System.out.println("Hovering Over: "+destination.getLatitude()+", "+destination.getLongitude());
    }
//cheking in each plane where does this method called from so we can chose the right getAbility
    //this depends on what can the plane do
    @Override
    public String getAbility() {
        if (Thread.currentThread().getStackTrace()[2].getFileName().equals("AttackMission.java"))
            return super.getAbility();
        else if (Thread.currentThread().getStackTrace()[2].getFileName().equals("IntelligenceMission.java"))
            return this.intelligencePlane.getAbility();
        else
            return this.verificationAttacksPlane.getAbility();
    }
}

