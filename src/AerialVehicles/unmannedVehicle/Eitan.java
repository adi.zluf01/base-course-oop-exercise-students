package AerialVehicles.unmannedVehicle;

import AerialVehicles.UnmannedAerialVehicle;
import AerialVehicles.planeTypes.AttackPlane;
import AerialVehicles.planeTypes.IntelligencePlane;
import Entities.*;
public class Eitan extends AttackPlane implements UnmannedAerialVehicle {
private IntelligencePlane intelligencePlane;
    public Eitan(int flightHour, Coordinates homeBase, Status status, int missilesAmount, MissileType missileType, SensorType sensor) {
        super(flightHour, homeBase, status, Family.HARON, missilesAmount, missileType);
        this.intelligencePlane = new IntelligencePlane(flightHour, homeBase, status,Family.HARON, sensor);
    }

    @Override
    public void hoverOverLocation(Coordinates destination) {
    this.setFlightStatus(Status.INTHEAIR);
    System.out.println("Hovering Over: "+destination.getLatitude()+", "+destination.getLongitude());
    }

    @Override
    public String getAbility(){
        if(Thread.currentThread().getStackTrace()[2].getFileName().equals("AttackMission.java"))
        return super.getAbility();
        else
            return this.intelligencePlane.getAbility();
    }
}
