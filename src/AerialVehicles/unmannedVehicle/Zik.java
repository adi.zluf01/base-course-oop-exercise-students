package AerialVehicles.unmannedVehicle;


import AerialVehicles.UnmannedAerialVehicle;
import AerialVehicles.planeTypes.IntelligencePlane;
import AerialVehicles.planeTypes.VerificationAttacksPlane;
import Entities.*;

public class Zik extends IntelligencePlane implements UnmannedAerialVehicle {
    private VerificationAttacksPlane verificationAttacksPlane;
    public Zik(int flightHour, Coordinates homeBase, Status status, SensorType sensor, CameraType camera) {
        super(flightHour, homeBase, status, Family.HERMES, sensor);
        this.verificationAttacksPlane=new VerificationAttacksPlane(flightHour,homeBase,status, Family.HERMES,camera);
    }

    @Override
    public void hoverOverLocation(Coordinates destination) {
        this.setFlightStatus(Status.INTHEAIR);
        System.out.println("Hovering Over: "+destination.getLatitude()+", "+destination.getLongitude());
    }
    @Override
    public String getAbility(){
        if(Thread.currentThread().getStackTrace()[2].getFileName().equals("IntelligenceMission.java"))
            return super.getAbility();
        else
            return this.verificationAttacksPlane.getAbility();
    }
}