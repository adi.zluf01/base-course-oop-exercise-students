package AerialVehicles.unmannedVehicle;

import AerialVehicles.UnmannedAerialVehicle;
import AerialVehicles.planeTypes.AttackPlane;
import AerialVehicles.planeTypes.IntelligencePlane;
import AerialVehicles.planeTypes.VerificationAttacksPlane;
import Entities.*;

public class Kochav extends AttackPlane implements UnmannedAerialVehicle {
    private IntelligencePlane intelligencePlane;
    private VerificationAttacksPlane verificationAttacksPlane;

    public Kochav(int flightHour, Coordinates homeBase, Status status, Family family, int missilesAmount, MissileType missileType, SensorType sensor, CameraType camera) {
        super(flightHour, homeBase, status, family, missilesAmount, missileType);
        this.intelligencePlane = new IntelligencePlane(flightHour, homeBase, status, family, sensor);
        this.verificationAttacksPlane = new VerificationAttacksPlane(flightHour, homeBase, status, family, camera);
    }

    @Override
    public void hoverOverLocation(Coordinates destination) {
        this.setFlightStatus(Status.INTHEAIR);
        System.out.println("Hovering Over: " + destination.getLatitude() + ", " + destination.getLongitude());
    }

    @Override
    public String getAbility() {
        if (Thread.currentThread().getStackTrace()[2].getFileName().equals("AttackMission.java"))
            return super.getAbility();
        else if (Thread.currentThread().getStackTrace()[2].getFileName().equals("IntelligenceMission.java"))
            return this.intelligencePlane.getAbility();
        else
            return this.verificationAttacksPlane.getAbility();
    }
}
