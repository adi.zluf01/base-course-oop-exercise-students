package AerialVehicles.fighterJets;


import AerialVehicles.planeTypes.AttackPlane;
import AerialVehicles.planeTypes.IntelligencePlane;
import Entities.*;

public class F15 extends AttackPlane {
    private IntelligencePlane intelligencePlane;

    public F15(int flightHour, Coordinates homeBase, Status status, int missilesAmount, MissileType missileType, SensorType sensor) {
        super(flightHour, homeBase, status, Family.ATTACKPLANE, missilesAmount, missileType);
        this.intelligencePlane = new IntelligencePlane(flightHour, homeBase, status, Family.ATTACKPLANE, sensor);
    }
    @Override
    public String getAbility(){
        if(Thread.currentThread().getStackTrace()[2].getFileName().equals("AttackMission.java"))
            return super.getAbility();
        else
            return this.intelligencePlane.getAbility();
    }
}
