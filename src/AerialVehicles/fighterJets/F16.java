package AerialVehicles.fighterJets;


import AerialVehicles.planeTypes.AttackPlane;
import AerialVehicles.planeTypes.VerificationAttacksPlane;
import Entities.*;

public class F16 extends AttackPlane {
    private VerificationAttacksPlane verificationAttacksPlane;
    public F16(int flightHour, Coordinates homeBase, Status status, int missilesAmount, MissileType missileType, CameraType camera) {
        super(flightHour, homeBase, status, Family.ATTACKPLANE, missilesAmount, missileType);
        verificationAttacksPlane=new VerificationAttacksPlane(flightHour,homeBase,status,Family.ATTACKPLANE,camera);
    }

    @Override
    public String getAbility(){
        if(Thread.currentThread().getStackTrace()[2].getFileName().equals("AttackMission.java"))
            return super.getAbility();
        else
            return this.verificationAttacksPlane.getAbility();
    }
}
