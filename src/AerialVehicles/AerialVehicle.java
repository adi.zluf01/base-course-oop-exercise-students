package AerialVehicles;

import Entities.*;
//the abstract class of aerial vehicles
public abstract class AerialVehicle {
    private int flightHour;
    private Coordinates homeBase;
    private Status flightStatus;
    private Family planeFamily;
//constructor
    public AerialVehicle(int flightHour, Coordinates homeBase, Status status, Family family) {
        this.flightHour = flightHour;
        this.homeBase = homeBase;
        this.flightStatus = status;
        this.planeFamily = family;
    }
//flyto method
    //checks the flight status and act from its value
    public void flyTo(Coordinates destination) {
        switch (this.flightStatus) {
            case READYFORFLIGHT:
                System.out.println("Ready! Flying to " + destination.getLatitude() + ", " + destination.getLongitude());
                this.flightStatus = Status.INTHEAIR;
                break;
            case INTHEAIR:
                System.out.println("Flying to " + destination.getLatitude() + ", " + destination.getLongitude());
                break;
            case NOTREADYFORFLIGHT:
                System.out.println("Aerial Vehicle isn't ready to fly");
                break;
        }
    }
// landing function
    //printing the plane is landing and checks whether it need a repair
    public void land(Coordinates destination) {
        System.out.println("Landing on " + destination.getLatitude() + ", " + destination.getLongitude());
        check();
    }
//checking whether a plane need a repair
    //by checking his type
    //this solution is the best for me because it easy to add kinds to your enum and then here and not to make more classes
    public void check() {
        switch (this.planeFamily) {
            case HARON:
                if (this.flightHour >= 150) {
                    this.flightStatus = Status.NOTREADYFORFLIGHT;
                    repair();
                } else {
                    this.flightStatus = Status.READYFORFLIGHT;
                }
                break;
            case HERMES:
                if (this.flightHour >= 100) {
                    this.flightStatus = Status.NOTREADYFORFLIGHT;
                    repair();
                } else {
                    this.flightStatus = Status.READYFORFLIGHT;
                }
                break;
            case ATTACKPLANE:
                if (this.flightHour >= 250) {
                    this.flightStatus = Status.NOTREADYFORFLIGHT;
                    repair();
                } else {
                    this.flightStatus = Status.READYFORFLIGHT;
                }
                break;
        }
    }

    public void repair() {
        this.flightHour = 0;
        this.flightStatus = Status.READYFORFLIGHT;
    }
//an abstract method for getting the ability of each type plane
    public abstract String getAbility();
//getters and setters
    public int getFlightHour() {
        return flightHour;
    }

    public void setFlightHour(int flightHour) {
        this.flightHour = flightHour;
    }

    public Coordinates getHomeBase() {
        return homeBase;
    }

    public void setHomeBase(Coordinates homeBase) {
        this.homeBase = homeBase;
    }

    public Status getFlightStatus() {
        return flightStatus;
    }

    public void setFlightStatus(Status flightStatus) {
        this.flightStatus = flightStatus;
    }

    public Family getPlaneFamily() {
        return planeFamily;
    }

    public void setPlaneFamily(Family planeFamily) {
        this.planeFamily = planeFamily;
    }

}
