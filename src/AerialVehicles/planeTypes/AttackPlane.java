package AerialVehicles.planeTypes;

import AerialVehicles.AerialVehicle;
import Entities.*;

import java.util.Locale;

public class AttackPlane extends AerialVehicle {
    private int missilesAmount;
    private MissileType missileType;

    public AttackPlane(int flightHour, Coordinates homeBase, Status status, Family family,int missilesAmount,MissileType missileType) {
        super(flightHour, homeBase, status, family);
        this.missilesAmount = missilesAmount;
        this.missileType = missileType;
    }

    public int getMissilesAmount() {
        return missilesAmount;
    }

    public void setMissilesAmount(int missilesAmount) {
        this.missilesAmount = missilesAmount;
    }

    public MissileType getMissileType() {
        return missileType;
    }

    public void setMissileType(MissileType missileType) {
        this.missileType = missileType;
    }
    //cheking in each plane where does this method called from so we can chose the right getAbility
    //this depends on what can the plane do
    @Override
    public String getAbility() {
        return this.missileType.toString().toLowerCase(Locale.ROOT) + "X" + this.missilesAmount;
    }
}
