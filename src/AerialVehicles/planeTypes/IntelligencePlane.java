package AerialVehicles.planeTypes;

import AerialVehicles.AerialVehicle;
import Entities.*;

import java.util.Locale;

public class IntelligencePlane extends AerialVehicle {
    protected SensorType sensor;
    public IntelligencePlane(int flightHour, Coordinates homeBase, Status status, Family family, SensorType sensor) {
        super(flightHour, homeBase, status, family);
        this.sensor = sensor;
    }

    public SensorType getSensor() {
        return sensor;
    }

    public void setSensor(SensorType sensor) {
        this.sensor = sensor;
    }
    //cheking in each plane where does this method called from so we can chose the right getAbility
    //this depends on what can the plane do
    @Override
    public String getAbility() {
        return "with sensor type: "+this.sensor.toString().toLowerCase(Locale.ROOT);
    }
}
