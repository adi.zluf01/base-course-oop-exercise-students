package AerialVehicles.planeTypes;

import AerialVehicles.AerialVehicle;
import Entities.*;

import java.util.Locale;

public class VerificationAttacksPlane extends AerialVehicle {
    protected CameraType camera;
    public VerificationAttacksPlane(int flightHour, Coordinates homeBase, Status status, Family family, CameraType camera) {
        super(flightHour, homeBase, status, family);
        this.camera = camera;
    }

    public CameraType getCamera() {
        return camera;
    }

    public void setCamera(CameraType camera) {
        this.camera = camera;
    }
    //cheking in each plane where does this method called from so we can chose the right getAbility
    //this depends on what can the plane do
    @Override
    public String getAbility() {
        return this.camera.toString().toLowerCase(Locale.ROOT) +" camera";
    }
}
